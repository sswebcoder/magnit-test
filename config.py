# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import os

root_path = os.path.dirname(__file__) + os.sep

db = {
    'host': 'localhost',
    'user': 'root',
    'password': '',
    'db': 'magnit_test',
    'charset': 'utf8',
    'use_unicode': True
}

template = {
    'path': root_path + 'html_tmpl' + os.sep,
    'ext': 'html'
}