/**
 * Created by phplamer on 02.01.15.
 */

'use strict';

util.addEvent(window, 'load', function () {
    //load regions
    util.ajax.get({
        url: '/region/listget.json',
        success: function (response) {
            showRegions(response.data);
        },
        error: function (response) {
            console.log('error: ', response.data);
        }
    });

    //listen submit event
    util.addEvent(document.getElementById('commentForm'), 'submit', submit);

    //hide validate error message on input
    var inputs = ['firstName', 'lastName', 'comment', 'email', 'contactPhone'];
    inputs.forEach(function (input) {
        util.addEvent(document.getElementById(input), 'input', function (e) {
            hideValidateError(e.target)
        });
    });


});

function showRegions(data) {
    addDataToSelect(data, 'region', selectRegion)
}

function selectRegion(e) {
    var regionId = e.target.value;
    //load city
    util.ajax.get({
        url: '/city/listget.json',
        data: {region_id: regionId},
        success: function (response) {
            showCitys(response.data);
        },
        error: function (response) {
            console.log('error: ', response.data);
        }
    });

}

function showCitys(data) {
    addDataToSelect(data, 'city')
}

function addDataToSelect(data, elementName, changeHandler) {
    data = data ? JSON.parse(data) : [];

    var select = document.getElementById(elementName);
    select.disabled = false;
    select.innerHTML = '';

    data.forEach(function (item) {
        var option = document.createElement('option');
        option.value = item.id;
        option.innerHTML = item.name;
        select.appendChild(option);
    });

    if (changeHandler) {
        util.addEvent(select, 'change', changeHandler);
        util.trigger(select, 'change');
    }
}

function submit(e) {
    e.stopPropagation();
    e.preventDefault();

    var errors = new Array();

    var data = {
        lastname: document.getElementById('lastName').value,
        firstname: document.getElementById('firstName').value,
        patronymic: document.getElementById('patronymic').value,
        region_id: document.getElementById('region').value,
        city_id: document.getElementById('city').value,
        contactphone: document.getElementById('contactPhone').value,
        email: document.getElementById('email').value,
        comment: document.getElementById('comment').value
    };

    if (!data.lastname) {
        errors.push({element: document.getElementById('lastName'), text: 'Поле "Фамилия" должно быть заполнено'});
    }
    if (!data.firstname) {
        errors.push({element: document.getElementById('firstName'), text: 'Поле "Имя" должно быть заполнено'});
    }
    if (!data.comment) {
        errors.push({element: document.getElementById('comment'), text: 'Необходимо ввести комментарий'});
    }

    var emailRegExp = /^[-.\w]+@[a-z\d]{2,}\.+[a-z]{2,6}$/i;
    if (data.email && !data.email.match(emailRegExp)) {
        errors.push({element: document.getElementById('email'), text: 'Введён некорректный email адрес'});
    }

    var phoneRegExp = /\([0-9]{3}\)[0-9]{7}/;
    if (data.contactphone && !data.contactphone.match(phoneRegExp)) {
        errors.push({
            element: document.getElementById('contactPhone'),
            text: 'Номер телефона введён некорректно. Введите телефон в формате: (код города)номер телефона. Например: (861)9999999'
        });
    }

    if (errors.length) {
        errors.forEach(function (error) {
            showValidateError(error.element, error.text);
        });
    } else {
        util.ajax.post({
            url: 'add.json',
            data: data,
            success: function (response) {
                showMessage('success',JSON.parse(response.data).message);
            },
            error: function (response) {
                showMessage('error',JSON.parse(response.data).message);
            }
        });
    }

    return false;
}


function showValidateError(element, text) {
    if (!element.parentElement.getElementsByClassName('notify')[0]) {
        util.addClass(element, 'validateError');
        var notifyDiv = document.createElement('div');
        notifyDiv.innerHTML = text;
        notifyDiv.className = 'notify';
        element.parentElement.appendChild(notifyDiv);
    }

}

function hideValidateError(element) {
    util.removeClass(element, 'validateError');
    if (element.parentElement.getElementsByClassName('notify')[0])
        element.parentElement.removeChild(element.parentElement.getElementsByClassName('notify')[0]);
}

function showMessage(type, text) {
    var message = document.createElement('div');
    if(type == 'success') {
        message.className = 'successMessage';
    } else {
        message.className = 'errorMessage';
    }

    message.innerHTML = text;
    var wrapper = document.getElementsByClassName('wrapper')[0];
    wrapper.innerHTML = '';
    wrapper.appendChild(message);
}