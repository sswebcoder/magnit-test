/**
 * Created by phplamer on 02.01.15.
 */

window.util = {

    addEvent: function (element, event, callback) {
        if (window.attachEvent) {
            element.attachEvent('on' + event, callback);
        } else {
            element.addEventListener(event, callback, false);
        }
    },

    removeEvent: function (element, event, callback) {
        if (window.detachEvent) {
            element.detachEvent('on' + event, callback);
        } else {
            element.removeEventListener(event, callback, false);
        }
    },

    trigger: function(element, eventName) {
        var event = new Event(eventName);
        element.dispatchEvent(event);
    },

    addClass: function(element, className) {
        var classArray = element.className.split(' ');
        classArray.push(className);
        element.className = classArray.join(' ');
    },

    removeClass: function(element, className) {
        var classArray = element.className.split(' ');
        classArray.splice(classArray.indexOf(className.trim()), 1);
        element.className = classArray.join(' ');
    },

    ajax: {
        req: function () {
            if (typeof XMLHttpRequest !== 'undefined') {
                return new XMLHttpRequest();
            }
            var versions = [
                "MSXML2.XmlHttp.5.0",
                "MSXML2.XmlHttp.4.0",
                "MSXML2.XmlHttp.3.0",
                "MSXML2.XmlHttp.2.0",
                "Microsoft.XmlHttp"
            ];

            var xhr;
            for (var i = 0; i < versions.length; i++) {
                try {
                    xhr = new ActiveXObject(versions[i]);
                    break;
                } catch (e) {
                }
            }
            return xhr;
        },

        send: function (url, success, error, method, data, sync) {
            var x = this.req();
            x.open(method, url, sync);
            x.onreadystatechange = function () {
                if (x.readyState == 4) {
                    var response = {
                        status: x.status,
                        statusText: x.statusText,
                        data: x.responseText
                    }
                    if(x.status == 200) {
                        success(response);
                    } else {
                        error(response);
                    }
                }
            };
            if (method == 'POST') {
                x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            }
            x.send(data)
        },

        get: function (params) {
            var url = params.url ? params.url : '/',
                data = params.data ? params.data : {},
                success = params.success ? params.success : function () {
                },
                error = params.error ? params.error : function () {
                },
                sync = params.sync ? params.sync : false;

            var query = [];
            for (var key in data) {
                query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
            }
            this.send(url + '?' + query.join('&'), success, error, 'GET', null, sync)
        },

        post: function (params) {
            var url = params.url ? params.url : '/',
                data = params.data ? params.data : {},
                success = params.success ? params.success : function () {
                },
                error = params.error ? params.error : function () {
                },
                sync = params.sync ? params.sync : false;

            var query = [];
            for (var key in data) {
                query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
            }
            this.send(url, success, error, 'POST', query.join('&'), sync)
        }
    }

}
