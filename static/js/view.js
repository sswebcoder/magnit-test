/**
 * Created by phplamer on 13.01.15.
 */

'use strict';

util.addEvent(window, 'load', function () {
    //load regions
    util.ajax.get({
        url: '/comment/listget.json',
        success: function (response) {
            showComments(JSON.parse(response.data));
        },
        error: function (response) {
            console.log('error: ', response.data);
        }
    });

});

function showComments(comments) {
    var commentList = document.getElementById('commentList');
    comments.forEach(function (comment) {
        commentList.appendChild(generateComment(comment));
    });

    var buttons = document.getElementsByTagName('button');
    for (var i in buttons) {
        if (Number.isInteger(Number(i))) {
            util.addEvent(buttons[i], 'click', deleteComment);
        }
    }
}

function generateComment(data) {
    var elements = new Array();
    var elementList = ['comment', 'header', 'name', 'date', 'content', 'remote'];

    elementList.forEach(function (element) {
        elements[element] = document.createElement('div');
        elements[element].className = element;
    });

    elements['name'].innerText = data.lastname + ' ' + data.firstname + ', ' + data.city_name;
    var date = document.createElement('span');
    date.innerText = data.create_date;
    elements['date'].appendChild(date);
    elements['header'].appendChild(elements['name']);
    elements['header'].appendChild(elements['date']);

    elements['content'].innerText = data.comment;
    elements['comment'].appendChild(elements['header']);
    elements['comment'].appendChild(elements['content']);

    var button = document.createElement('button');
    button.value = data.id;
    button.innerText = 'Удалить';
    elements['remote'].appendChild(button);
    elements['comment'].appendChild(elements['remote']);
    return elements['comment'];
}

function deleteComment(e) {
    var id = e.target.value;
    util.ajax.post({
        url: '/comment/del.json',
        data: {id: id},
        success: function (response) {
            hideComment(e.target.parentNode.parentNode);
            console.log(response.data);
        },
        error: function (response) {
            console.log('error: ', response.data);
            alert(JSON.parse(response.data).message);

        }
    });

}

function hideComment(comment) {
    comment.parentNode.removeChild(comment);
}
