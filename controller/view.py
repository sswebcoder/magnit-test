# -*- coding: utf-8 -*-
__author__ = 'phplamer'

from cview.view import *
#import model.comment as model
import urllib
import json


def start(actionName, queryData):
    print('view')
    status = '404 Not Found'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'Not found'
    if not actionName:
        status, headers, content = index()
#    elif actionName.lower() == 'delete.json':
#        status, headers, content = delete(queryData)
    return status, headers, content


def index():
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'content from view'
    view = ViewView({'test': 'proverka', 'mytest':'Немного кирилицы '})
    content = view.render()
    return status, headers, content
