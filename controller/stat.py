# -*- coding: utf-8 -*-
__author__ = 'phplamer'

from cview.stat import *
from model.comment import *
import urllib
import json


def start(actionName, queryData):
    print('view')
    status = '404 Not Found'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'Not found'
    if not actionName:
        status, headers, content = index()
    elif actionName.lower() == 'region.html':
        status, headers, content = region(queryData)
    return status, headers, content


def index():
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    comment = ModelComment()
    regions = comment.countByRegions()
    table = ''
    for region in regions:
        table += '<tr>'
        table += '<td><a href="/stat/region.html?id=' + region['region_id'] + '">' + region['region_name'] + '</a></td>'
        table += '<td>' + region['count'] + '</td>'
        table += '</tr>'

    view = ViewStat({'table': table, 'title': 'Статистика по регионам', 'columnName':'Регион'})
    content = view.render()
    return status, headers, content


def region(queryData):
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    comment = ModelComment()
    citys = comment.countByCityInRegion(queryData['id'])
    table = ''
    for city in citys:
        table += '<tr>'
        table += '<td>' + city['city_name'] + '</td>'
        table += '<td>' + city['count'] + '</td>'
        table += '</tr>'

    view = ViewStat({'table': table, 'title': 'Статистика по городам', 'columnName':'Город'})
    content = view.render()
    return status, headers, content
