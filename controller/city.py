# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import model.city as model
import json


def start(actionName, queryData):
    status = '404 Not Found'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'Not found'
    if not actionName:
        status, headers, content = index()
    elif actionName.lower() == 'listget.json':
        status, headers, content = listget(queryData)
    return status, headers, content


def index(tmplPath):
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'City'
    return status, headers, content


def listget(queryData):
    status = '200 OK'
    headers = [('Content-type', 'application/json; charset=UTF-8')]
    city_model = model.City()
    if 'region_id' in queryData:
        content = json.dumps(city_model.get_all(queryData['region_id']))
    else:
        content = json.dumps(city_model.get_all(''))
    return status, headers, content