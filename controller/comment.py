# -*- coding: utf-8 -*-
__author__ = 'phplamer'

from cview.comment import *
from model.comment import *
#import model.comment as model
import urllib
import json


def start(actionName, queryData):
    status = '404 Not Found'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'Not found'

    if not actionName:
        status, headers, content = index()
    elif actionName.lower() == 'add.json':
        status, headers, content = add(queryData)
    elif actionName.lower() == 'listget.json':
        status, headers, content = listget()
    elif actionName.lower() == 'del.json':
        status, headers, content = delete(queryData)
    return status, headers, content


def index():
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    view = ViewComment()
    return status, headers, view.render()


def add(queryData):
    status = '200 OK'
    headers = [('Content-type', 'application/json; charset=UTF-8')]
    comment = ModelComment()
    for attr_name in queryData:
        comment.attributes[attr_name] = urllib.unquote(queryData[attr_name])
    if comment.save():
        content = json.dumps({'message': 'Комментарий успешно добавлен'})
    else:
        status = '400 Bad Request'
        content = json.dumps({'message': 'Во время добавления комментария возникли ошибки'})
    return status, headers, content


def listget():
    comment = ModelComment()
    #comment.get_all()
    status = '200 OK'
    headers = [('Content-type', 'application/json; charset=UTF-8')]
    content = json.dumps(comment.get_all())
    return status, headers, content


def delete(queryData):
    comment = ModelComment()
    status = '200 OK'
    headers = [('Content-type', 'application/json; charset=UTF-8')]
    if comment.delete(queryData['id']):
        content = json.dumps({'message': 'Комментарий успешно удалён'})
    else:
        content = json.dumps({'message': 'Комментарий не удалён. Возникли ошибки...'})

    return status, headers, content