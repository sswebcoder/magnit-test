# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import json
import model.region as model


def start(actionName, queryData):
    status = '404 Not Found'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'Not found'

    if not actionName:
        status, headers, content = index()
    elif actionName.lower() == 'listget.json':
        status, headers, content = get_list()
    return status, headers, content


def index():
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=UTF-8')]
    content = 'regions'
    return status, headers, content


def get_list():
    status = '200 OK'
    headers = [('Content-type', 'application/json; charset=UTF-8')]
    region = model.Region()
    content = json.dumps(region.get_all())
    return status, headers, content