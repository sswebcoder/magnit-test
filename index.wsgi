# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.dirname(__file__))
import router


def application(environ, start_response):
    handler = router.Router(environ)
    response_status = handler.get_response_status()
    response_headers = handler.get_response_headers()
    start_response(response_status, response_headers)
    if environ['mod_wsgi.process_group'] != '':
        import signal, os
        os.kill(os.getpid(), signal.SIGINT)

    return handler.get_response_content()



