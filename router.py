# -*- coding: utf-8 -*-
__author__ = 'phplamer'

from cStringIO import StringIO


class Router:
    def __init__(self, environ):
        self.queryData = {}
        self.setRouteParams(environ)
        request_method = environ['REQUEST_METHOD'].lower()

        if (request_method == 'get'):
            self.parseQuery(environ['QUERY_STRING'])
        else:
            query_length = int(environ.get('CONTENT_LENGTH', '0'))
            post_body = StringIO(environ['wsgi.input'].read(query_length))
            for query_string in post_body.readlines():
                self.parseQuery(query_string)

        try:
            controller = __import__('controller.' + self.controllerName, fromlist=[''])
        except ImportError as error:
            self.response_status = '404 Not Found'
            self.response_headers = [('Content-type', 'text/html; charset=UTF-8')]
            self.response_content = 'Not found'
        else:
            self.response_status, self.response_headers, self.response_content = controller.start(self.actionName,
                                                                                                  self.queryData)

    def get_response_status(self):
        return self.response_status

    def get_response_headers(self):
        return self.response_headers

    def get_response_content(self):
        return self.response_content

    def setRouteParams(self, environ):
        url = environ['PATH_INFO'].strip()
        if url == '/':
            self.controllerName = 'index'
            self.actionName = 'default'
        else:
            urlPathArr = url.split('/')
            self.controllerName = urlPathArr[1]
            self.actionName = 'default' if urlPathArr[len(urlPathArr) - 1] == urlPathArr[1] else urlPathArr[
                len(urlPathArr) - 1]

    def parseQuery(self, data):
        #print(data)
        if not data:
            return ''

        for pair in data.split('&'):
            tmp = pair.split('=')
            key = tmp[0]
            value = tmp[1] if len(tmp) == 2 else ''
            self.queryData[key] = value
        #print(self.queryData)