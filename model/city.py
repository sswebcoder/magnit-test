# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import MySQLdb
import config
from model.main import *


class City(Main):
    def __init__(self):
        self.db = MySQLdb.connect(host=config.db['host'], user=config.db['user'], passwd=config.db['password'], db=config.db['db'], charset=config.db['charset'], use_unicode=config.db['use_unicode'])
        self.db_cursor = self.db.cursor()

    def __del__(self):
        self.db.close()

    def get_all(self, region_id):
        try:
            region_id = int(region_id)
        except ValueError:
            self.db_cursor.execute('SELECT id, city_name, region_id FROM city')
        else:
            self.db_cursor.execute('SELECT id, city_name, region_id FROM city WHERE region_id=%s', (region_id))

        data = self.db_cursor.fetchall()
        return self.mysql_array_assoc(('id', 'name', 'region_id'), data)
