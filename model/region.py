# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import MySQLdb
import config
from model.main import *


class Region(Main):
    def __init__(self):
        self.db = MySQLdb.connect(host=config.db['host'], user=config.db['user'], passwd=config.db['password'], db=config.db['db'], charset=config.db['charset'], use_unicode=config.db['use_unicode'])
        self.db_cursor = self.db.cursor()

    def __del__(self):
        self.db.close()

    def get_all(self):
        self.db_cursor.execute('SELECT id, region_name FROM region')
        data = self.db_cursor.fetchall()
        return self.mysql_array_assoc(('id', 'name'), data)


def mysql_array_assoc(indexes, source_obj):
    dest_list = []

    for row in source_obj:
        i = 0
        assoc = {}
        while(i < len(indexes)):
            assoc[indexes[i]] = str(row[i])
            i += 1
        dest_list.append(assoc)
    return dest_list
