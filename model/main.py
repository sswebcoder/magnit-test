# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import MySQLdb
import config


class Main(object):
    def __init__(self):
        self.db = MySQLdb.connect(host=config.db['host'], user=config.db['user'], passwd=config.db['password'],
                                  db=config.db['db'], charset=config.db['charset'],
                                  use_unicode=config.db['use_unicode'])
        self.db_cursor = self.db.cursor()

    def __del__(self):
        self.db.close()

    def mysql_array_assoc(self, indexes, source_obj):
        dest_list = []

        for row in source_obj:
            i = 0
            assoc = {}
            while (i < len(indexes)):
                assoc[indexes[i]] = str(row[i])
                i += 1
            dest_list.append(assoc)
        return dest_list

