# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import MySQLdb
import config
from model.main import *


class ModelComment(Main):
    def __init__(self):
        super(ModelComment, self).__init__()
        self.attributes = {}

    def validate(self):
        return True

    def save(self):
        if self.validate():
            self.db_cursor.execute("""INSERT INTO comment(firstname, lastname, patronymic, region_id, city_id, contactphone, email, comment)
            VALUES('%(firstname)s', '%(lastname)s', '%(patronymic)s', '%(region_id)s', '%(city_id)s', '%(contactphone)s', '%(email)s', '%(comment)s')"""
                                   % {'firstname': self.attributes['firstname'],
                                      'lastname': self.attributes['lastname'],
                                      'patronymic': self.attributes['patronymic'],
                                      'region_id': self.attributes['region_id'],
                                      'city_id': self.attributes['city_id'],
                                      'contactphone': self.attributes['contactphone'],
                                      'email': self.attributes['email'],
                                      'comment': self.attributes['comment']})
            self.db.commit()
            return True
        return False

    def get_all(self):
        self.db_cursor.execute("""SELECT comment.id, firstname, lastname, `comment`, create_date, city.city_name FROM comment
                LEFT JOIN city ON comment.city_id=city.id ORDER BY create_date DESC ;""")
        data = self.db_cursor.fetchall()
        return self.mysql_array_assoc(('id', 'firstname', 'lastname', 'comment', 'create_date', 'city_name'), data)

    def delete(self, id):
        try:
            self.db_cursor.execute("DELETE FROM comment WHERE id=%s", (id))
            self.db.commit()
        except MySQLdb.Error:
            return False
        else:
            return True

    def countByRegions(self):
        self.db_cursor.execute("""SELECT region.region_name, region.id, COUNT(region_id) as count FROM comment
                                  LEFT JOIN region ON comment.region_id=region.id GROUP BY comment.region_id;""")
        data = self.db_cursor.fetchall()
        return self.mysql_array_assoc(('region_name', 'region_id', 'count'), data)

    def countByCityInRegion(self, id):
        self.db_cursor.execute("""SELECT city.city_name, COUNT(city_id) as count FROM comment
                                  LEFT JOIN city ON comment.city_id=city.id WHERE comment.region_id=%s GROUP BY city_id """, (id))
        data = self.db_cursor.fetchall()
        return self.mysql_array_assoc(('city_name', 'count'), data)
