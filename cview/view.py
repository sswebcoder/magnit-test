# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import config
import re


class ViewView:
    def __init__(self, tmplData=''):
        self.tmplPath = config.template['path']
        self.tmplData = tmplData
        self.tmplName = 'view.html'

    def render(self):
        ptrn = re.compile(r'\{\{\$(\w+)\}\}', re.MULTILINE)
        file = open(self.tmplPath + self.tmplName, 'r')
        fileContent = file.read()
        matches = ptrn.search(fileContent)
        while matches:

            if matches.group(1) in self.tmplData:
                fileContent = ptrn.sub(self.tmplData[matches.group(1)], fileContent, 1)
            else:
                fileContent = ptrn.sub('', fileContent, 1)
                print(fileContent)
            matches = ptrn.search(fileContent)

        return fileContent
