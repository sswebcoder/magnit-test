# -*- coding: utf-8 -*-
__author__ = 'phplamer'

import config


class ViewComment:
    def __init__(self):
        self.tmplPath = config.template['path']
        self.tmplData = ''
        self.tmplName = 'comment.html'

    def render(self):
        file = open(self.tmplPath + self.tmplName, 'r')
        fileContent = file.read()
        return fileContent
