# Test #

### Web server host configuration ###

For allow access to static files add in host configuration next directives.

Example for apache:

    Alias /static/ /pathToTest/static/

    <Directory /pathToTest/static>
	Require all granted
    </Directory>